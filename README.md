# Stalker Portal APIV1 Extention #

https://wiki.infomir.eu/eng/ministra-tv-platform/ministra-setup-guide/rest-api-v1

### Summary ###

* Adding new available resources

* 0.0.1

* Author: Sergey Volyanytsky sergey.volyanytsky@gmail.com

### Installation ###

* Put the files into stalker_portal/server/lib/resapi/v1/ directory

### Resources ###

* streamers

* storages

* channels

* channel_links